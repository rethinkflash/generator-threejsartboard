function DefaultModel(params) {
	this.geometry = new THREE.BoxGeometry(10,10,10);
	this.material = new THREE.MeshNormalMaterial({
		color: 0xffffff
	});
	THREE.Mesh.call(this,this.geometry,this.material);
}

DefaultModel.prototype = Object.create(THREE.Mesh.prototype);
