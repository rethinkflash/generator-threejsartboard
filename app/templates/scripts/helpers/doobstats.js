DoobStats = function() {
	this.statsFPS = new Stats();
	this.statsFPS.setMode(0);
	this.statsFPS.domElement.style.position = 'absolute';
	this.statsFPS.domElement.style.left = '0px';
	this.statsFPS.domElement.style.top = '0px';
	document.body.appendChild(this.statsFPS.domElement);
}

DoobStats.prototype = {
	constructor: DoobStats,

	begin:function() {
		this.statsFPS.begin();
	},

	end:function() {
		this.statsFPS.end();
	}
}