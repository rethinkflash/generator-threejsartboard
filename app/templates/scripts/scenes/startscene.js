function StartScene(params) {
	THREE.Scene.call(this);

	//your models
	this.defaultModel = new DefaultModel();
	this.add(this.defaultModel);

	//your lights
	var intensity = 2.5;
	var distance = 100;
	var c1 = 0xff0040;
	this.ambiLight = new THREE.AmbientLight(0x111111);
	this.light1 = new THREE.PointLight(c1, intensity, distance);

	this.add(this.ambiLight);
	this.add(this.light1);
};

StartScene.prototype = Object.create(THREE.Scene.prototype);

StartScene.prototype.onRenderUpdate = function() {
	this.defaultModel.rotation.x += 0.06;
	this.defaultModel.rotation.y += 0.05;
}