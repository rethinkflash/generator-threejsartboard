function JSONLoaderStart(src) {
	THREE.Scene.call(this);

	var self = this;

	var loader = new THREE.JSONLoader();
	loader.load(src, function (geometry, materials) {
		this.material = new THREE.MeshNormalMaterial({
			color: 0xffffff
		});
		self.mesh = new THREE.Mesh(geometry, this.material);
		self.mesh.scale.multiplyScalar(50);
		self.add(self.mesh);
	});
}

JSONLoaderStart.prototype = Object.create(THREE.Scene.prototype);

JSONLoaderStart.prototype.onRenderUpdate = function() {
	if(!this.mesh)
		return;
	this.mesh.rotation.x += 0.006;
	this.mesh.rotation.y += 0.005;
}