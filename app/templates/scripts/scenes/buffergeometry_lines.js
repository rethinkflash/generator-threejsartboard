function BufferGeometryLines(params) {
	THREE.Scene.call(this);

	var segments = 100;

	var geometry = new THREE.BufferGeometry();
	var material = new THREE.LineBasicMaterial({
		vertexColors: THREE.VertexColors
	});

	this.positions = new Float32Array(segments*3);
	this.colors = new Float32Array(segments*3);

	var r = 800;

	for(var i = 0; i < segments; i++) {
		var x = Math.random() * r - r / 2;
		var y = Math.random() * r - r / 2;
		var z = Math.random() * r - r / 2;

		//positions
		this.positions[ i * 3 ] = x;
		this.positions[ i * 3 + 1 ] = y;
		this.positions[ i * 3 + 2 ] = z;

		//colors
		this.colors[ i * 3 ] = (x / r) + 0.5;
		this.colors[ i * 3 + 1 ] = (y / r) + 0.5;
		this.colors[ i * 3 + 2 ] = (z / r) + 0.5;
	}

	geometry.addAttribute( 'position', new THREE.BufferAttribute(this.positions, 3));
	geometry.addAttribute( 'color', new THREE.BufferAttribute(this.colors, 3));

	geometry.computeBoundingSphere();

	this.mesh = new THREE.Line(geometry, material);

	this.add(this.mesh);
}

BufferGeometryLines.prototype = Object.create(THREE.Scene.prototype);

BufferGeometryLines.prototype.addSceneGUI = function(topGui) {

}

BufferGeometryLines.prototype.onRenderUpdate = function() {
	var time = Date.now() * 0.001;

	this.mesh.rotation.x = time * 0.25;
	this.mesh.rotation.y = time * 0.5;
}