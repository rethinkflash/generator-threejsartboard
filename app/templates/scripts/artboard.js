function ThreeJSArtboard(params) {

  var enableGUI = params.guiFlag;
  if(enableGUI) {
    this.doobStats = new DoobStats();
  }

  console.log('ThreeJSArtboard initialized!');

  this.canvasEle = params.canvas;

  this.renderer = new THREE.WebGLRenderer({canvas:this.canvasEle});
  // this.renderer.setClearColor(0xeeeeee,1);

  this.camera = new THREE.PerspectiveCamera(75, params.cssWidth/params.cssHeight, 0.1,5000);
  this.camera.position.z = 100;

  this.controls = new THREE.OrbitControls(this.camera);
  this.controls.damping = 0.2;

  this.scene = new StartScene();

  this.gridhelper = new THREE.GridHelper(200,10);
  this.gridhelper.position.y = -150;
  this.gridhelper.setColors('#00000ff','#808080');

  this.scene.add(this.gridhelper);

  this.resize(null);

  this.render();
}

ThreeJSArtboard.prototype = {

  constructor: ThreeJSArtboard,

  resize:function(e) {
    var cssWidth = window.innerWidth;
    var cssHeight = window.innerHeight;
    this.canvasEle.width = cssWidth;
    this.canvasEle.height = cssHeight;
    this.renderer.setSize(cssWidth, cssHeight);
    this.camera.aspect = cssWidth/cssHeight;
    this.camera.updateProjectionMatrix();
  },

  render:function() {


    if(this.doobStats)
      this.doobStats.begin();

    //main onenterframe code STARTS here

    this.scene.onRenderUpdate();
    // this.camera.lookAt(this.scene.defaultModel.position);
    this.renderer.render(this.scene, this.camera);
    // console.log(this.scene.defaultModel.rotation);
    //main onenterframe code ENDS here

    if(this.doobStats)
      this.doobStats.end();

    requestAnimationFrame(this.render.bind(this));
  }
}
