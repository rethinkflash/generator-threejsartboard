var $ = shoestring;
$(document).ready(function() {
  var artboard = new ThreeJSArtboard({
    canvas:$('#artboard')[0],
    cssWidth:window.innerWidth,
    cssHeight:window.innerHeight,
    guiFlag:true
  });
  $(window).bind('resize',function(e){
    artboard.resize();
  });
});
