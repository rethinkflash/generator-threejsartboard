module.exports = function (grunt) {
  'use strict';

  //Project configuration.
  grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),

    /**
     * Project Paths Configuration
     * ===============================
     */

    paths: {
      projectName: '<%= threejsProjectName %>',
      //images folder name
      images: 'img',
      //where to store built files
      build: 'build',
      //sources
      src: 'app',
      //main email file
      srcIndex: 'index.html',
      //this is the default development domain
      devDomain: 'http://<%%= connect.options.hostname %>:<%%= connect.options.port %>/'
    },

    /**
     * Watch Tasks
     * ===============================
     */

    watch: {
      livereload: {
        options: {
            livereload: '<%%= connect.options.livereload %>'
        },
        files: [
            '<%%= paths.src %>/<%%= paths.srcIndex %>',
            '<%%= paths.src %>/css/{,*/}*.css',
            '<%%= paths.src %>/<%%= paths.images %>/{,*/}*.{png,jpg,jpeg,gif}',
            '<%%= paths.src %>/scripts/**/*.js',
        ]
      }
    },

    /**
     * Server Tasks
     * ===============================
     */
    connect: {
      options: {
          open: '<%%= paths.devDomain %>',
          hostname: 'localhost',
          livereload: 35729,
          port: 8022
      },
      dev: {
          options: {
              base: '<%%= paths.src %>'
          }
      },
      // build: {
      //   options: {
      //       base: '<%%= paths.build %>'
      //   }
      // }
    },

    /**
     * Cleanup Tasks
     * ===============================
     */
    clean: {
        // buildStart: ['<%%= paths.build %>/index.html'],
        // buildFinish: ['<%%= paths.build %>/css/','<%%= paths.build %>/pre-compile.html'],
        // buildImage: ['<%%= paths.build %>/img/']
    },

    /**
     * Image Optimize build Task
     * ===============================
     */
    imageoptim: {
      build:{
        options: {
          jpegMini: false,
          imageAlpha: false,
          quitAfter: false
        },
        src: ['<%%= paths.src %>/img/**/*.jpg']
      }
    },

    /**
     * Uglify build Task
     * ===============================
     */

    uglify: {
      options: {
        mangle: false,
        beautify: false,
        preserveComents: false
      },
      build: {
            files: {
              'build/scripts/artboard.min.js': [
                'app/scripts/vendor/shoestring/shoestring.js',
                'app/scripts/vendor/statsjs/stats.js',
                'app/scripts/vendor/threejs/three.min.js',
                'app/scripts/artboard.js',
                'app/scripts/main.js',

              ],
            },
          }
        },

    /**
     * Notification Tasks
     * ===============================
     */
    notify: {
      notify_hooks: {
        options: {
          enabled: true,
          title: '<%%= paths.projectname %>'
        }
      },
      dev: {
        options: {
          message: 'Dev Environment Ready!'
        }
      }
    },
   });

  [
    'grunt-contrib-connect',
    'grunt-contrib-watch',
    'grunt-contrib-clean',
    'grunt-contrib-copy',
    'grunt-contrib-uglify',
    'grunt-imageoptim',
    'grunt-notify',

  ].forEach(grunt.loadNpmTasks);

  grunt.registerTask('default', 'dev');

  grunt.registerTask('dev',[
    'connect:dev',
    'notify:dev',
    'watch',
  ]);

  grunt.registerTask('bower',[
    'bowercopy',
  ]);

  grunt.registerTask('build',[
    'copy:build'
  ]);

  grunt.registerTask('build:view',[
    'uglify:build',
    // 'connect:build',
    // 'watch'
  ]);

  grunt.registerTask('imgopti', [
    'imageoptim:build'
  ]);
}
