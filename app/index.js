'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var ThreejsartboardGenerator = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the kickass Threejsartboard generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'threejsProjectName',
      message: 'What would you like to name your project?',
      default: 'My ThreeJS project'
    }];

    this.prompt(prompts, function (props) {
      this.threejsProjectName = props.threejsProjectName;

      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      this.dest.mkdir('app');
      this.dest.mkdir('app/templates');
      this.dest.mkdir('app/css');
      this.dest.mkdir('app/scripts');

      this.template('_package.json', 'package.json');
      this.template('_Gruntfile.js', 'Gruntfile.js');

      this.template('_index.html', 'app/index.html');
      this.template('_buildindex.html', 'app/build.html');
      this.src.copy('css/styles.css','app/css/styles.css');
      this.directory('scripts/', 'app/scripts');
    },

    projectfiles: function () {
      this.src.copy('editorconfig', '.editorconfig');
      this.src.copy('jshintrc', '.jshintrc');
    }
  },

  end: function () {
    this.installDependencies();
  }
});

module.exports = ThreejsartboardGenerator;
